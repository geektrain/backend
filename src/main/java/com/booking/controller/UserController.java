package com.booking.controller;

import com.booking.dto.StringResponse;
import com.booking.dto.UserDto;
import com.booking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.UUID;

@RestController
@RequestMapping(value = "/users", consumes = "application/json", produces = "application/json")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping()
    public Iterable<UserDto> getAll() {
        return userService.getAll();
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public UserDto save(@RequestBody final UserDto dto) {
        return userService.save(dto);
    }

    @RequestMapping(value = "/getName", method = RequestMethod.GET)
    public StringResponse getName() {
        for (int i = 0; i < 1000000; i++) {
            UUID uuid = UUID.randomUUID();
        }
        return userService.getResponse();
    }


    @RequestMapping(value = "/saveName", method = RequestMethod.POST)
    public StringResponse getName(@RequestBody StringResponse response) {
        userService.setResponse(response);
        return userService.getResponse();
    }
}
