package com.booking.dao;

import com.booking.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class UserRepository {

    @Autowired
    private EntityManager entityManager;

    public List<UserEntity> getAll() {
        return entityManager
                .createQuery("SELECT u from UserEntity u", UserEntity.class)
                .getResultList();

    }

    public UserEntity save(UserEntity entity) {
        entityManager.persist(entity);
        return entity;
    }


}
