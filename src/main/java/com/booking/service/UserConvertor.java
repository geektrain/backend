package com.booking.service;

import com.booking.dto.UserDto;
import com.booking.entity.UserEntity;
import org.springframework.stereotype.Service;

@Service
public class UserConvertor {

    public UserDto convert(UserEntity userEntity) {
        return UserDto.builder()
                .name(userEntity.getName())
                .lastName(userEntity.getLastName())
                .eMail(userEntity.getEMail())
                .id(userEntity.getId())
                .build();
    }

}
