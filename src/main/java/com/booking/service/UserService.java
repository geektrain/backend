package com.booking.service;

import com.booking.dao.UserRepository;
import com.booking.dto.StringResponse;
import com.booking.dto.UserDto;
import com.booking.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.annotation.SessionScope;

import java.util.Objects;
import java.util.stream.Collectors;


@Service
@SessionScope
public class UserService {
    private StringResponse response;

    @Autowired
    private UserConvertor convertor;

    @Autowired
    private UserRepository repository;

    public Iterable<UserDto> getAll() {
        return repository.getAll().stream()
                .map(entity -> convertor.convert(entity))
                .collect(Collectors.toList());
    }

    @Transactional
    public UserDto save(UserDto dto) {
        UserEntity userEntity = UserEntity.builder()
                .name(dto.getName())
                .lastName(dto.getLastName())
                .eMail(dto.getEMail())
                .build();

        return convertor.convert(repository.save(userEntity));
    }

    public StringResponse getResponse() {
        return Objects.isNull(response) ? new StringResponse() : response;

    }

    public void setResponse(StringResponse response) {
        if (Objects.isNull(response)) {
            response = new StringResponse();
        }
        this.response = response;
    }
}
