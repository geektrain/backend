FROM java:8

#EXPOSE 8080

ADD target/booking.jar booking.jar

# start the Payara server
ENTRYPOINT ["java","-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=8080","-jar","booking.jar"]
